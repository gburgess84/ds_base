//
// Created by ivaughn on 5/19/21.
//
#ifndef DS_ASIO_HELPERS_H
#define DS_ASIO_HELPERS_H

#include "ds_asio/ds_asio.h"

namespace ds_asio {

/// Load a standard set of parameters for a matcher function
/// @param nh A ROS node handle to load parameters from
/// @param conn_name The connection name to load parameters for
/// @return The matcher function to use
MatchFunction parseMatchFunction(const ros::NodeHandle& nh, const std::string& conn_name);

} // namespace ds_asio

#endif //DS_ASIO_HELPERS_H
